# WASM build of KCFcpp project

*See below for the original project readme*

This is a fork of the [KCFcpp](https://github.com/joaofaro/KCFcpp) project to build a [WebAssembly](https://webassembly.org/) module to be used in [Video annotation project](https://www.robots.ox.ac.uk/~vgg/software/vvt/), for bringing the object tracking capability to the browser.

The aim of this project is to serve as an example of converting an existing CPP
project into WebAssembly module and package KCFcpp to be used in other JS projects.

Check [gitlab-ci.yml](.gitlab-ci.yml) to see how this is built. The build output is a single JS (~1.5 MB) file that can be imported into your own project. Check the [Gitlab Packages](https://gitlab.com/vgg/KCFcpp/-/packages/2058479) to grab a copy!

Check [src/tracking_wasm.cpp](./src/tracking_wasm.cpp) to see how emscripten is used to create an interface for JS to communicate with the WASM module. Check out the comprehensive [Emscripten docs](https://emscripten.org/docs/introducing_emscripten/index.html) for what's possible.

This project was tweaked to be compatible with OpenCV 4 and was modified only to create the WebAssembly module. Please use the original repo to use the tracker directly to get outputs.

*NPM Packaging coming soon*

### Building the project

While you can directly use the JS package built, you can make your own
modifications and build it too!

You will need:
- git
- [`git-lfs`](https://github.com/git-lfs/git-lfs/wiki/Installation)
- docker

*Check [`gitlab-ci.yml`](.gitlab-ci.yml) to see how to build.*

# C++ KCF Tracker
This package includes a C++ class with several tracking methods based on the Kernelized Correlation Filter (KCF) [1, 2].   
It also includes an executable to interface with the VOT benchmark.

[1] J. F. Henriques, R. Caseiro, P. Martins, J. Batista,   
"High-Speed Tracking with Kernelized Correlation Filters", TPAMI 2015.

[2] J. F. Henriques, R. Caseiro, P. Martins, J. Batista,   
"Exploiting the Circulant Structure of Tracking-by-detection with Kernels", ECCV 2012.


Authors: Joao Faro, Christian Bailer, Joao F. Henriques   
Contacts: joaopfaro@gmail.com, Christian.Bailer@dfki.de, henriques@isr.uc.pt   
Institute of Systems and Robotics - University of Coimbra / Department of Augmented Vision DFKI   

### Algorithms (in this folder) ###

"KCFC++", command: ./KCF   
Description: KCF on HOG features, ported to C++ OpenCV. The original Matlab tracker placed 3rd in VOT 2014.

"KCFLabC++", command: ./KCF lab   
Description: KCF on HOG and Lab features, ported to C++ OpenCV. The Lab features are computed by quantizing CIE-Lab colors into 15 centroids, obtained from natural images by k-means.   

The CSK tracker [2] is also implemented as a bonus, simply by using raw grayscale as features (the filter becomes single-channel).   

### Compilation instructions ###
There are no external dependencies other than OpenCV 3.0.0. Tested on a freshly installed Ubuntu 14.04.   

1) cmake CMakeLists.txt   
2) make   

### Running instructions ###

The runtracker.cpp is prepared to be used with the VOT toolkit. The executable "KCF" should be called as:   

./KCF [OPTION_1] [OPTION_2] [...]

Options available:   

gray - Use raw gray level features as in [1].   
hog - Use HOG features as in [2].   
lab - Use Lab colorspace features. This option will also enable HOG features by default.   
singlescale - Performs single-scale detection, using a variable-size window.   
fixed_window - Keep the window size fixed when in single-scale mode (multi-scale always used a fixed window).   
show - Show the results in a window.   

To include it in your project, without the VOT toolkit you just need to:
	
	// Create the KCFTracker object with one of the available options
	KCFTracker tracker(HOG, FIXEDWINDOW, MULTISCALE, LAB);

	// Give the first frame and the position of the object to the tracker
	tracker.init( Rect(xMin, yMin, width, height), frame );

	// Get the position of the object for the new frame
	result = tracker.update(frame);
