#include <string>
#include <opencv2/core.hpp>
#include <opencv2/imgproc.hpp>
#include <emscripten/bind.h>
#include "kcftracker.hpp"
#include <memory>

using namespace emscripten;

class _Tracker {
private:
    cv::Rect roi;
    int rows;
    int cols;
    bool status;
    std::unique_ptr<KCFTracker> tracker;

    cv::Mat getMatFromTypedArray(const emscripten::val& typedArray, const unsigned int length) {
        auto frame = cv::Mat(this->rows, this->cols, CV_8UC4);
        auto dst = cv::Mat(this->rows, this->cols, CV_8UC3);
        emscripten::val memoryView{emscripten::typed_memory_view(length, frame.data)};
        memoryView.call<void>("set", typedArray);
        cv::cvtColor(frame, dst, cv::COLOR_RGBA2BGR);
        return dst;
    }
public:
    _Tracker(
            const emscripten::val& typedArray,
            const unsigned int rows,
            cv::Rect rec) {

        this->status = true;
        this->roi = rec;
        tracker.reset(new KCFTracker(
            true,  // HOG
            true,  // FIXEDWINDOW
            true,  // MULTISCALE
            true   // LAB
        ));

        const unsigned int length = typedArray["length"].as<unsigned int>();
        const unsigned int cols = length / rows / 4;
        
        this->rows = rows;
        this->cols = cols;
        auto dst = this->getMatFromTypedArray(typedArray, length);     
        tracker->init(roi, dst);
    }
    
    cv::Rect track_object(const emscripten::val& typedArray) {
        const unsigned int length = typedArray["length"].as<unsigned int>();
        auto dst = this->getMatFromTypedArray(typedArray, length);
        this->roi = tracker->update(dst);
        return this->roi;
    }

    cv::Rect get_roi() const { return this->roi; }
    int get_rows() const { return this->rows; }
    int get_cols() const { return this->cols; }
    bool get_status() const { return this->status; }
};

EMSCRIPTEN_BINDINGS(my_module) {
    emscripten::value_object<cv::Rect_<int>> ("Rect") \
        .field("x", &cv::Rect_<int>::x) \
        .field("y", &cv::Rect_<int>::y) \
        .field("width", &cv::Rect_<int>::width) \
        .field("height", &cv::Rect_<int>::height);

    emscripten::class_<_Tracker>("Tracker")
        .constructor<const emscripten::val&, const unsigned int, cv::Rect >()
        .function("track_object", &_Tracker::track_object)
    	.property("roi", &_Tracker::get_roi)
    	.property("rows", &_Tracker::get_rows)
    	.property("cols", &_Tracker::get_cols)
    	.property("status", &_Tracker::get_status);
}
