# OpenCV Emscripten build without pthreads

The tar.gz archive was created by running the script `build.sh` present in this folder
inside a container created from the emscripen docker image [`emscripten/emsdk:2.0.16`](https://hub.docker.com/layers/emscripten/emsdk/2.0.16/images/sha256-6ad6c2ae911ffe17059fc9a20848647e6b10823f5306e0275e29e79eb9f305b6?context=explore).

The opencv 4.5.2 version was compiled without pthread support to make the resulting WASM work in Safari, since
[sharedarraybuffer is disabled in Safari](https://caniuse.com/?search=sharedarraybuffer) due
to Spectre vulnerabilities. Check `deps/` folder for steps to build.


## Requirements

- docker
- git
- `build.sh` and `cmake_pthread.patch` from the repo

## Steps to reproduce

```bash
#!/bin/bash

KCF_REPO=  # SET THIS VARIABLE TO POINT TO THE KCFcpp REPO

# Clone opencv
git clone --depth 1 --branch 4.5.2 https://github.com/opencv/opencv.git

# Clone contrib modules
cd opencv/
git clone --depth 1 --branch 4.5.2 https://github.com/opencv/opencv_contrib.git contrib

# Patch cmake file to disable pthread so that it works on Safari
git apply ${KCF_REPO}/deps/cmake_pthread.patch

# Copy the script to build
cp ${KCF_REPO}/deps/build.sh .

# Pull emscripten docker image
docker pull emscripten/emsdk:2.0.16  # or use the sha - emscripten/emsdk@sha256:6ad6c2ae911ffe17059fc9a20848647e6b10823f5306e0275e29e79eb9f305b6

# Run the container
docker run --rm -it -u $(id -u):$(id -g) -v ${PWD}:/code -w /code emscripten/emsdk:2.0.16 bash build.sh

# This will create an install directory which contains the minimum build.
# Optionally, Create a tar of your build
# tar -zcf opencv_4.5.2_nopthread_emscripten.tar.gz -C install/ bin/ include/ lib/ share/
```